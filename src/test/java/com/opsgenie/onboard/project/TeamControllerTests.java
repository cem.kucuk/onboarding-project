package com.opsgenie.onboard.project;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.opsgenie.onboard.project.controller.TeamController;
import com.opsgenie.onboard.project.model.Team;
import com.opsgenie.onboard.project.service.TeamServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TeamController.class)
public class TeamControllerTests {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TeamServiceImpl teamService;

    @Test
    public void testGetAllTeamsIfEmpty() throws Exception {
        List<Team> outputArray = Collections.emptyList();
        when(teamService.listAllTeams()).thenReturn(outputArray);
        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(outputArray);

        mockMvc.perform(get("/teams"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(jsonOutput));
    }

    @Test
    public void testGetAllTeams() throws Exception {
        Team expectedTeam1 = new Team("id1", "team1", Arrays.asList("user1", "user3"));
        Team expectedTeam2 = new Team("id2", "team2", Arrays.asList("user3", "user0"));
        List<Team> outputArray = Arrays.asList(expectedTeam1, expectedTeam2);
        when(teamService.listAllTeams()).thenReturn(outputArray);
        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(outputArray);

        mockMvc.perform(get("/teams"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(jsonOutput));
    }

    @Test
    public void testGetTeam() throws Exception {
        Team expectedTeam = new Team("id1", "team1", Arrays.asList("user1", "user3"));
        when(teamService.getTeam("id1")).thenReturn(expectedTeam);
        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(expectedTeam);

        mockMvc.perform(get("/teams/id1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(jsonOutput));
    }

    @Test
    public void testCreateTeam() throws Exception {
        Team addTeamRequest = new Team(null, "team1", Arrays.asList("user1", "user3"));
        Team expectedTeam = new Team("id1", "team1", Arrays.asList("user1", "user3"));
        when(teamService.createTeam(addTeamRequest)).thenReturn(expectedTeam);

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonRequest = objectWriter.writeValueAsString(addTeamRequest);
        String jsonOutput = objectWriter.writeValueAsString(expectedTeam);

        mockMvc.perform(post("/teams")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(jsonOutput));
    }

    /*
    @Test
    public void testUpdateTeam() throws Exception {
        Team oldTeam = new Team("id1", "newTeam", Arrays.asList("user1", "user3"));
        Team expectedTeam = new Team("id1", "newTeam", Arrays.asList("user1", "user3"));
        when(teamService.updateTeam(oldTeam)).thenReturn(expectedTeam);

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonRequest = objectWriter.writeValueAsString(oldTeam);
        String jsonOutput = objectWriter.writeValueAsString(expectedTeam);

        mockMvc.perform(put("/teams/id1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(jsonOutput));
    }
    */

}
