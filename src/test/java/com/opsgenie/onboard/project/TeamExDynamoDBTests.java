package com.opsgenie.onboard.project;

import com.opsgenie.onboard.project.accessor.DynamoDBAccessor;
import com.opsgenie.onboard.project.model.Team;
import com.opsgenie.onboard.project.repo.TeamRepository;
import com.opsgenie.onboard.project.repo.TeamRepositoryDynamoDBImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(MockitoJUnitRunner.class)
public class TeamExDynamoDBTests {

    private TeamRepository teamRepository;

    @Before
    public void setUp(){
        DynamoDBAccessor<Team> dynamoDBAccessor = new DynamoDBAccessor<>();
        dynamoDBAccessor.init();
        teamRepository = new TeamRepositoryDynamoDBImpl(dynamoDBAccessor);

    }

    @After
    public void tearDown(){
        teamRepository = null;
    }

    @Test
    public void exTestCreate() throws Exception{
        Team team = new Team("id", "name", Collections.emptyList());
        Team addedTeam = teamRepository.createTeam(team);
        assertThat(addedTeam, is(equalTo(team)));
    }
}
