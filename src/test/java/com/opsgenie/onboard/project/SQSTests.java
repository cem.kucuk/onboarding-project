package com.opsgenie.onboard.project;


import com.opsgenie.onboard.project.accessor.SQSAccessor;
import com.opsgenie.onboard.project.dto.MessageDTO;
import com.opsgenie.onboard.project.repo.TeamRepositoryImpl;
import com.opsgenie.onboard.project.service.TeamServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SQSTests {

    @InjectMocks
    private TeamServiceImpl teamService;

    @Mock
    private SQSAccessor sqsAccessor;

    @Mock
    private TeamRepositoryImpl teamRepository;

    private String teamId = "teamId";

    @Test
    public void assignTeamWithMultipleUserIds() throws Exception {
        List<String> userIds = Arrays.asList("user1", "user2");
        teamService.assignTeamMember(teamId, userIds);
        for (String userId : userIds) {
            verify(sqsAccessor).sendMessage(new MessageDTO(teamId, userId));
        }
        verifyNoMoreInteractions(sqsAccessor);
    }

    @Test
    public void assignTeamWithEmptyUserIds() throws Exception {
        teamService.assignTeamMember(teamId, Collections.emptyList());
        verifyZeroInteractions(sqsAccessor);
    }


    @Test
    public void assignTeamWithIOException() throws Exception {
        doThrow(new IOException("some problem"))
                .when(sqsAccessor).sendMessage(new MessageDTO(teamId, "user2"));

        List<String> userIds = Arrays.asList("user1", "user2", "user3");
        try {
            teamService.assignTeamMember(teamId, userIds);
            fail("My method didn't throw when I expected it to");
        } catch (IOException ignored) {
        }
        verify(sqsAccessor, times(2)).sendMessage(any());
    }
}
