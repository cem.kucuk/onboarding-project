package com.opsgenie.onboard.project;

import com.opsgenie.onboard.project.model.Team;
import com.opsgenie.onboard.project.repo.TeamRepositoryImpl;
import com.opsgenie.onboard.project.service.TeamServiceImpl;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TeamServiceTests {

    @InjectMocks
    private TeamServiceImpl teamService;

    @Mock
    private TeamRepositoryImpl teamRepository;

    @After
    public void tearDown() {
        teamRepository = null;
        teamService = null;
    }

    @Test
    public void listTeamsTest() throws Exception {
        final List<Team> expectedTeams = Arrays.asList(
                new Team("id1", "team1", Arrays.asList("user1", "user3")),
                new Team("id2", "team2", Arrays.asList("user3", "user0"))
        );

        when(teamRepository.listAllTeams())
                .thenReturn(expectedTeams);

        final List<Team> teams = teamService.listAllTeams();

        assertThat(teams, is(equalTo(expectedTeams)));

        verify(teamRepository, times(1)).listAllTeams();
    }

    @Test
    public void createTeamTest() throws Exception {
        Team newTeam = new Team("", "team1", Arrays.asList("user1", "user3"));
        teamService.createTeam(newTeam);

        ArgumentCaptor<Team> captor = ArgumentCaptor.forClass(Team.class);
        verify(teamRepository).createTeam(captor.capture());

        assertThat(captor.getValue().getId(), not(isEmptyString()));
    }

    @Test
    public void updateTeam() throws Exception {
        Team oldTeam = new Team("id1", "team1", Arrays.asList("user1", "user3"));
        Team newTeam = new Team("id1", "newTeam1", Collections.emptyList());
        when(teamRepository.updateTeam(newTeam))
                .thenReturn(oldTeam);
        final Team team = teamService.updateTeam(newTeam);
        assertThat(team, is(equalTo(oldTeam)));
    }


}
