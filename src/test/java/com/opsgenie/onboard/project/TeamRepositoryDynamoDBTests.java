package com.opsgenie.onboard.project;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.opsgenie.onboard.project.accessor.DynamoDBAccessor;
import com.opsgenie.onboard.project.model.Team;
import com.opsgenie.onboard.project.model.User;
import com.opsgenie.onboard.project.repo.TeamRepositoryDynamoDBImpl;
import com.opsgenie.onboard.project.repo.UserRepositoryDynamoDBImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class TeamRepositoryDynamoDBTests {

    @InjectMocks
    private TeamRepositoryDynamoDBImpl teamRepository;

    @InjectMocks
    private UserRepositoryDynamoDBImpl userRepository;

    @Mock
    private DynamoDBAccessor dynamoDBAccessor;

    @Test
    public void listAllTeamsTest() {
        Team expectedTeam1 = new Team("id1", "team1", Arrays.asList("user1", "user3"));
        Team expectedTeam2 = new Team("id2", "team2", Arrays.asList("user3", "user0"));
        List<Team> expectedTeams = Arrays.asList(expectedTeam1, expectedTeam2);

        when(dynamoDBAccessor.scan(eq(Team.class), any(DynamoDBScanExpression.class))).thenReturn(expectedTeams);

        List<Team> teams = teamRepository.listAllTeams();

        assertThat(teams, is(equalTo(expectedTeams)));
        verify(dynamoDBAccessor, times(1)).scan(eq(Team.class), any(DynamoDBScanExpression.class));
    }

    @Test
    public void getTeam() {
        Team expectedTeam = new Team("id", "team",Collections.emptyList());

        when(dynamoDBAccessor.load(Team.class, "id")).thenReturn(expectedTeam);

        Team team = teamRepository.getTeam("id");
        assertThat(team, is(equalTo(expectedTeam)));
        verify(dynamoDBAccessor, times(1)).load(Team.class, "id");
    }

    @Test
    public void listAllUsersTest() {
        User expectedUser1 = new User("uid1","user1");
        User expectedUser2 = new User("uid2","user2");
        List<User> expectedUsers = Arrays.asList(expectedUser1,expectedUser2);

        when(dynamoDBAccessor.scan(eq(User.class), any(DynamoDBScanExpression.class))).thenReturn(expectedUsers);

        List<User> users = userRepository.listAllUsers();

        assertThat(users, is(equalTo(expectedUsers)));
        verify(dynamoDBAccessor, times(1)).scan(eq(User.class), any(DynamoDBScanExpression.class));

    }

    @Test
    public void getUser() {
        User expectedUser = new User("id", "user");

        when(dynamoDBAccessor.load(User.class, "id")).thenReturn(expectedUser);

        User user = userRepository.getUser("id");
        assertThat(user, is(equalTo(expectedUser)));
        verify(dynamoDBAccessor, times(1)).load(User.class, "id");
    }

}

