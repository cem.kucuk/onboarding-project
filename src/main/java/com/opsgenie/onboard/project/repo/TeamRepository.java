package com.opsgenie.onboard.project.repo;

import com.opsgenie.onboard.project.model.Team;

import java.util.List;

public interface TeamRepository {
    Team createTeam(Team team);
    Team getTeam(String id);
    Team updateTeam(Team team);
    void deleteTeam(Team team);
    List<Team> listAllTeams();
}
