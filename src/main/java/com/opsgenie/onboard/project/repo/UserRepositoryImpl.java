package com.opsgenie.onboard.project.repo;

import com.opsgenie.onboard.project.model.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Qualifier("Local")
@Repository
public class UserRepositoryImpl implements UserRepository {

    private Map<String, User> allUsers = new HashMap<>();

    @Override
    public User createUser(User user) {
        allUsers.put(user.getId(), user);
        return user;
    }

    @Override
    public User getUser(String id) {
        return allUsers.get(id);
    }

    @Override
    public User updateUser(User user) {
        allUsers.get(user.getId()).setName(user.getName());
        return allUsers.get(user.getId());
    }

    @Override
    public void deleteUser(User user) {
        allUsers.remove(user.getId());
    }

    @Override
    public List<User> listAllUsers() {
        return new ArrayList<>(allUsers.values());
    }
}
