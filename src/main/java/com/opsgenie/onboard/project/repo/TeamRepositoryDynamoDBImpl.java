package com.opsgenie.onboard.project.repo;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.opsgenie.onboard.project.accessor.DynamoDBAccessor;
import com.opsgenie.onboard.project.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Qualifier("Dynamo")
@Repository
public class TeamRepositoryDynamoDBImpl implements TeamRepository {

    private DynamoDBAccessor<Team> dynamoDBAccessor;

    @Autowired
    public TeamRepositoryDynamoDBImpl(DynamoDBAccessor<Team> dynamoDBAccessor) {
        this.dynamoDBAccessor = dynamoDBAccessor;
    }


    @Override
    public Team createTeam(Team team) {
        dynamoDBAccessor.save(team);
        return team;
    }

    @Override
    public Team getTeam(String id) {
        return dynamoDBAccessor.load(Team.class, id);
    }

    @Override
    public Team updateTeam(Team team) {
        dynamoDBAccessor.save(team);
        return team;
    }

    @Override
    public void deleteTeam(Team team) {
        dynamoDBAccessor.delete(team);
    }

    @Override
    public List<Team> listAllTeams() {
        return dynamoDBAccessor.scan(Team.class, new DynamoDBScanExpression());
    }
}

/*
var params = {
    TableName: 'Onboard_teams',
    KeySchema: [ // The type of of schema.  Must start with a HASH type, with an optional second RANGE.
        { // Required HASH type attribute
            AttributeName: 'id',
            KeyType: 'HASH',
        }
    ],
    AttributeDefinitions: [ // The names and types of all primary and index key attributes only
        {
            AttributeName: 'id',
            AttributeType: 'S',
        },
    ],
    ProvisionedThroughput: { // required provisioned throughput for the table
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10,
    }
};
dynamodb.createTable(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});
*/