package com.opsgenie.onboard.project.repo;

import com.opsgenie.onboard.project.model.Team;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Qualifier("Local")
@Repository
public class TeamRepositoryImpl implements TeamRepository {

    private Map<String, Team> allTeams = new HashMap<>();

    @Override
    public Team createTeam(Team team) {
        allTeams.put(team.getId(), team);
        return team;
    }

    @Override
    public Team getTeam(String id) {
        return allTeams.get(id);
    }

    @Override
    public Team updateTeam(Team team) {
        allTeams.get(team.getId()).setName(team.getName());
        allTeams.get(team.getId()).setUserIds(team.getUserIds());
        return allTeams.get(team.getId());
    }

    @Override
    public void deleteTeam(Team team) {
        allTeams.remove(team.getId());
    }

    @Override
    public List<Team> listAllTeams() {
        return new ArrayList<>(allTeams.values());
    }
}
