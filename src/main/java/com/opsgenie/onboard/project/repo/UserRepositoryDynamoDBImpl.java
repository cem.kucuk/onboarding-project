package com.opsgenie.onboard.project.repo;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.opsgenie.onboard.project.accessor.DynamoDBAccessor;
import com.opsgenie.onboard.project.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Qualifier("Dynamo")
@Repository
public class UserRepositoryDynamoDBImpl implements UserRepository {

    private DynamoDBAccessor<User> dynamoDBAccessor;

    @Autowired
    public UserRepositoryDynamoDBImpl(DynamoDBAccessor<User> dynamoDBAccessor) {
        this.dynamoDBAccessor = dynamoDBAccessor;
    }

    @Override
    public User createUser(User user) {
        dynamoDBAccessor.save(user);
        return user;
    }

    @Override
    public User getUser(String id) {
        return dynamoDBAccessor.load(User.class, id);
    }

    @Override
    public List<User> listAllUsers() {
        return dynamoDBAccessor.scan(User.class, new DynamoDBScanExpression());
    }

    @Override
    public User updateUser(User user) {
        dynamoDBAccessor.save(user);
        return user;
    }

    @Override
    public void deleteUser(User user) {
        dynamoDBAccessor.delete(user);
    }
}


/*
var params = {
    TableName: 'Onboard_users',
    KeySchema: [ // The type of of schema.  Must start with a HASH type, with an optional second RANGE.
        { // Required HASH type attribute
            AttributeName: 'id',
            KeyType: 'HASH',
        }
    ],
    AttributeDefinitions: [ // The names and types of all primary and index key attributes only
        {
            AttributeName: 'id',
            AttributeType: 'S',
        },
    ],
    ProvisionedThroughput: { // required provisioned throughput for the table
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10,
    }
};
dynamodb.createTable(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});
*/
