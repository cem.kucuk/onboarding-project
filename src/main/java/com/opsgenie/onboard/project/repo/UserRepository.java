package com.opsgenie.onboard.project.repo;

import com.opsgenie.onboard.project.model.User;

import java.util.List;

public interface UserRepository {
    User createUser(User user);
    User getUser(String id);
    User updateUser(User user);
    void deleteUser(User user);
    List<User> listAllUsers();
}
