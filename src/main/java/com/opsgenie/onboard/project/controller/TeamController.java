package com.opsgenie.onboard.project.controller;

import com.opsgenie.onboard.project.dto.TeamDTO;
import com.opsgenie.onboard.project.model.Team;
import com.opsgenie.onboard.project.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/teams")
public class TeamController {

    private final TeamService teamService;

    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public TeamDTO createTeam(@RequestBody TeamDTO teamDTO) {
        Team tempTeam = TeamDTO.to(teamDTO);
        Team newTeam = teamService.createTeam(tempTeam);
        return TeamDTO.from(newTeam);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<TeamDTO> getAllTeams() {
        return TeamDTO.convertAll(teamService.listAllTeams());
    }

    @RequestMapping(value = "/{teamId}",method = RequestMethod.GET)
    public TeamDTO getTeam(@PathVariable String teamId) {
        Team team = teamService.getTeam(teamId);
        if (team == null) {
            throw new RuntimeException("Team not found with identifier [" + teamId + "]");
        }
        return TeamDTO.from(team);
    }

    @RequestMapping(value = "/{teamId}",method = RequestMethod.PUT)
    public TeamDTO updateTeam(@RequestBody TeamDTO team, @PathVariable String teamId) {
        Team oldTeam = teamService.getTeam(teamId);
        if (oldTeam == null) {
            throw new RuntimeException("Team not found with identifier [" + teamId + "]");
        }
        oldTeam.setName(team.getName());
        oldTeam.setUserIds(team.getUserIds());
        Team updatedTeam = teamService.updateTeam(oldTeam);
        return TeamDTO.from(updatedTeam);
    }

    @RequestMapping(value = "/{teamId}",method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity deleteTeam(@PathVariable String teamId) {
        Team deletedTeam = teamService.getTeam(teamId);
        if (deletedTeam == null) {
            throw new RuntimeException("Team not found with identifier [" + teamId + "]");
        }
        teamService.deleteTeam(deletedTeam);
        return ResponseEntity.ok().body("deleted");
    }

    @RequestMapping(value = "/{teamId}/assignTeamMember", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity assignTeamMember(@PathVariable String teamId, @RequestBody List<String> userIds) {
        Team team = teamService.getTeam(teamId);
        if (team == null) {
            throw new RuntimeException("Team not found with identifier [" + teamId + "]");
        }
        try {
            teamService.assignTeamMember(teamId, userIds);
        } catch (IOException e) {
            return ResponseEntity.badRequest().body("");
        }
        return ResponseEntity.accepted().body("ACCEPTED");
    }

}
