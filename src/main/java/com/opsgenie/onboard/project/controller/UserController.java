package com.opsgenie.onboard.project.controller;


import com.opsgenie.onboard.project.dto.UserDTO;
import com.opsgenie.onboard.project.model.User;
import com.opsgenie.onboard.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public UserDTO createUser(@RequestBody UserDTO userDTO) {
        User tempUser = UserDTO.to(userDTO);
        User newUser = userService.createUser(tempUser);
        return UserDTO.from(newUser);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<UserDTO> getAllUsers() {
        return UserDTO.convertAll(userService.listAllUsers());
    }

    @RequestMapping(value = "/{userId}",method = RequestMethod.GET)
    public UserDTO getUser(@PathVariable String userId) {
        User user = userService.getUser(userId);
        if (user == null) {
            throw new RuntimeException("User not found with identifier [" + userId + "]");
        }
        return UserDTO.from(user);
    }

    @RequestMapping(value = "/{userId}",method = RequestMethod.PUT)
    public UserDTO updateUser(@RequestBody UserDTO user, @PathVariable String userId) {
        User oldUser = userService.getUser(userId);
        if (oldUser == null) {
            throw new RuntimeException("User not found with identifier [" + userId + "]");
        }
        oldUser.setName(user.getName());
        User updatedUser = userService.updateUser(oldUser);
        return UserDTO.from(updatedUser);
    }

    @RequestMapping(value = "/{userId}",method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity deleteUser(@PathVariable String userId) {
        User deletedUser = userService.getUser(userId);
        if (deletedUser == null) {
            throw new RuntimeException("User not found with identifier [" + userId + "]");
        }
        userService.deleteUser(deletedUser);
        return ResponseEntity.ok().body("deleted");
    }

}
