package com.opsgenie.onboard.project.accessor;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;

@Component
public class DynamoDBAccessor<T> {

    private AmazonDynamoDB client;

    @PostConstruct
    public void init() {
        AWSCredentials awsCredentials = new BasicAWSCredentials("fake", "fake");
        AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration("http://localhost:3966", "us-west-2"))
                .withCredentials(awsCredentialsProvider).build();
    }

    @PreDestroy
    public void destroy() {
        if (client != null) {
            client.shutdown();
        }
    }

    public AmazonDynamoDB getClient() {
        return client;
    }

    public void setClient(AmazonDynamoDB client) {
        this.client = client;
    }

    public List<T> scan(Class<T> clazz, DynamoDBScanExpression scanExpression) {
        DynamoDBMapper mapper = getDynamoDBMapper();
        return new ArrayList<>(mapper.scan(clazz, scanExpression));
    }

    public T load(Class<T> clazz, Object hashKey) {
        DynamoDBMapper mapper = getDynamoDBMapper();
        return mapper.load(clazz, hashKey);
    }

    public void save(T clazz) {
        DynamoDBMapper mapper = getDynamoDBMapper();
        mapper.save(clazz);
    }

    public void delete(T entity) {
        DynamoDBMapper mapper = getDynamoDBMapper();
        mapper.delete(entity);
    }

    private DynamoDBMapper getDynamoDBMapper() {
        DynamoDBMapperConfig mapperConfig = DynamoDBMapperConfig.builder()
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE)
                .withConsistentReads(DynamoDBMapperConfig.ConsistentReads.CONSISTENT)
                .build();

        return new DynamoDBMapper(client, mapperConfig);
    }
}
