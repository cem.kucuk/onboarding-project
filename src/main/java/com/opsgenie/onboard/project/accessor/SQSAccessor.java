package com.opsgenie.onboard.project.accessor;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opsgenie.onboard.project.dto.MessageDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@Configuration
@PropertySource("aws.properties")
public class SQSAccessor {

    private AmazonSQS client;

    private String queueUrl;

    @Value("${aws.secretKey}")
    private String secretKey;

    @Value("${aws.accessKey}")
    private String accessKey;

    @Value("${aws.queueName}")
    private String queueName;

    @PostConstruct
    public void init() {
        AmazonSQSClientBuilder clientBuilder = AmazonSQSClientBuilder.standard();
        AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        clientBuilder.setRegion("eu-west-1");
        clientBuilder.setCredentials(awsCredentialsProvider);
        client = clientBuilder.build();
        queueUrl = client.getQueueUrl(queueName).getQueueUrl();
    }

    public void sendMessage(MessageDTO messageDTO) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonOutput = objectMapper.writeValueAsString(messageDTO);
        SendMessageRequest newMessage = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(jsonOutput);
        client.sendMessage(newMessage);
    }

    public List<MessageDTO> receiveMessage() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<MessageDTO> messageDTOs = new ArrayList<>();
        List<Message> messages = client.receiveMessage(queueUrl).getMessages();
        for (Message m : messages) {
            messageDTOs.add(objectMapper.readValue(m.getBody(), MessageDTO.class));
            client.deleteMessage(queueUrl, m.getReceiptHandle());
        }
        return messageDTOs;
    }
}
