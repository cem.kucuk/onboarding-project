package com.opsgenie.onboard.project.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class MessageDTO {
    @JsonProperty("teamId")
    private String teamId;

    @JsonProperty("userId")
    private String userId;

    public MessageDTO() {
    }

    public MessageDTO(String teamId, String userId) {
        this.teamId = teamId;
        this.userId = userId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageDTO that = (MessageDTO) o;
        return Objects.equals(teamId, that.teamId) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(teamId, userId);
    }

    @Override
    public String toString() {
        return "MessageDTO{" +
                "teamId='" + teamId + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
