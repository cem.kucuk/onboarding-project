package com.opsgenie.onboard.project.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opsgenie.onboard.project.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserDTO {
    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    public UserDTO() {
    }

    @JsonIgnore
    public UserDTO(String id, String name){
        this.id = id;
        this.name = name;
    }

    @JsonIgnore
    public static UserDTO from(User user){
        return new UserDTO(user.getId(), user.getName());
    }

    @JsonIgnore
    public static User to(UserDTO userDTO) {
        return new User(userDTO.getId(), userDTO.getName());
    }

    @JsonIgnore
    public static List<UserDTO> convertAll(List<User> allUsers){
        List<UserDTO> allUserDTOs = new ArrayList<>();
        for (User u : allUsers) {
            allUserDTOs.add(from(u));
        }
        return  allUserDTOs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
