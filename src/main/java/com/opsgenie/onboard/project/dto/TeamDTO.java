package com.opsgenie.onboard.project.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opsgenie.onboard.project.model.Team;

import java.util.ArrayList;
import java.util.List;

public class TeamDTO {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("userIds")
    private List<String> userIds;


    public TeamDTO(){
    }

    @JsonIgnore
    public TeamDTO(String id, String name, List<String> userIds) {
        this.id = id;
        this.name = name;
        this.userIds = new ArrayList<String>(userIds);
    }

    @JsonIgnore
    public static TeamDTO from(Team team){
        return new TeamDTO(team.getId(), team.getName(), team.getUserIds());
    }

    @JsonIgnore
    public static Team to(TeamDTO teamDTO) {
        return new Team(teamDTO.getId(), teamDTO.getName(), teamDTO.getUserIds());
    }

    @JsonIgnore
    public static List<TeamDTO> convertAll(List<Team> allTeams){
        List<TeamDTO> allTeamDTOs = new ArrayList<>();
        for (Team t : allTeams) {
            allTeamDTOs.add(from(t));
        }
        return allTeamDTOs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }
}
