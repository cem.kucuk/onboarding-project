package com.opsgenie.onboard.project.service;

import com.opsgenie.onboard.project.model.User;

import java.util.List;

public interface UserService {
    User createUser(User user);
    User getUser(String id);
    List<User> listAllUsers();
    User updateUser(User user);
    void deleteUser(User user);
}
