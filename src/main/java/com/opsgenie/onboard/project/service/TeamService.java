package com.opsgenie.onboard.project.service;

import com.opsgenie.onboard.project.dto.MessageDTO;
import com.opsgenie.onboard.project.model.Team;

import java.io.IOException;
import java.util.List;

public interface TeamService {
    Team createTeam(Team team);
    Team getTeam(String id);
    List<Team> listAllTeams();
    Team updateTeam(Team team);
    void deleteTeam(Team team);

    void assignTeamMember(String teamId, List<String> userIds) throws IOException;

    void addMember(MessageDTO messageDTO);
}
