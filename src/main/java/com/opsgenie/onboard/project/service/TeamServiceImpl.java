package com.opsgenie.onboard.project.service;

import com.opsgenie.onboard.project.accessor.SQSAccessor;
import com.opsgenie.onboard.project.dto.MessageDTO;
import com.opsgenie.onboard.project.model.Team;
import com.opsgenie.onboard.project.repo.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.UUID;


@Service
public class TeamServiceImpl implements TeamService{

    private TeamRepository repo;
    private SQSAccessor sqsAccessor;


    @Autowired
    public TeamServiceImpl(@Qualifier("Dynamo") TeamRepository repo, SQSAccessor sqsAccessor) {
        this.repo = repo;
        this.sqsAccessor = sqsAccessor;
    }

    @Override
    public Team createTeam(Team team) {
        team.setId(UUID.randomUUID().toString());
        return repo.createTeam(team);
    }

    @Override
    public Team getTeam(String id) {
        return repo.getTeam(id);
    }

    @Override
    public List<Team> listAllTeams() {
        return repo.listAllTeams();
    }

    @Override
    public Team updateTeam(Team team) {
        return repo.updateTeam(team);
    }

    @Override
    public void deleteTeam(Team team) {
        repo.deleteTeam(team);
    }

    @Override
    public void assignTeamMember(String teamId, List<String> userIds) throws IOException {
        for (String u : userIds) {
            MessageDTO messageDTO = new MessageDTO(teamId, u);
            sqsAccessor.sendMessage(messageDTO);
        }
    }

    @Override
    public void addMember(MessageDTO messageDTO) {
        Team team = getTeam(messageDTO.getTeamId());
        if (!team.getUserIds().contains(messageDTO.getUserId()))
            team.getUserIds().add(messageDTO.getUserId());
        updateTeam(team);
    }
}
