package com.opsgenie.onboard.project.service;

import com.opsgenie.onboard.project.model.User;
import com.opsgenie.onboard.project.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService{

    private UserRepository repo;

    @Autowired
    public UserServiceImpl(@Qualifier("Dynamo") UserRepository repo) {
        this.repo = repo;
    }

    @Override
    public User createUser(User user) {
        user.setId(UUID.randomUUID().toString());
        return repo.createUser(user);
    }

    @Override
    public User getUser(String id) {
        return repo.getUser(id);
    }

    @Override
    public User updateUser(User user) {
        return repo.updateUser(user);
    }

    @Override
    public void deleteUser(User user) {
        repo.deleteUser(user);
    }

    @Override
    public List<User> listAllUsers() {
        return repo.listAllUsers();
    }
}
