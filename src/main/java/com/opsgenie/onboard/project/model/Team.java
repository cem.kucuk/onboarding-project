package com.opsgenie.onboard.project.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@DynamoDBTable(tableName = "Onboard_teams")
public class Team {

    private String id;
    private String name;
    private List<String> userIds;

    public Team() {
    }

    public Team(String id, String name, List<String> userIds) {
        this.id = id;
        this.name = name;
        this.userIds = new ArrayList<String>(userIds);
    }

    @DynamoDBHashKey(attributeName = "id")
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    @DynamoDBAttribute(attributeName = "name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @DynamoDBAttribute(attributeName = "userIds")
    public List<String> getUserIds() {
        return userIds;
    }
    public void setUserIds(List<String> userIds) {
        this.userIds = new ArrayList<String>(userIds);
    }

    @DynamoDBIgnore
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return Objects.equals(id, team.id) &&
                Objects.equals(name, team.name) &&
                Objects.equals(userIds, team.userIds);
    }

    @DynamoDBIgnore
    @Override
    public int hashCode() {
        return Objects.hash(id, name, userIds);
    }

    @DynamoDBIgnore
    @Override
    public String toString() {
        return "Team{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", userIds=" + userIds +
                '}';
    }
}
