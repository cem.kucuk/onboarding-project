package com.opsgenie.onboard.project.processor;

import com.opsgenie.onboard.project.accessor.SQSAccessor;
import com.opsgenie.onboard.project.dto.MessageDTO;
import com.opsgenie.onboard.project.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class AssignTeamMemberQueueProcessor {

    private SQSAccessor sqsAccessor;
    private TeamService teamService;

    @Autowired
    public AssignTeamMemberQueueProcessor(SQSAccessor sqsAccessor, TeamService teamService) {
        this.sqsAccessor = sqsAccessor;
        this.teamService = teamService;
    }

    @Scheduled(fixedDelay = 1000)
    private void pollMembers() throws IOException {
        List<MessageDTO> messageDTOs = sqsAccessor.receiveMessage();
        for (MessageDTO m : messageDTOs) {
            teamService.addMember(m);
        }
    }

}
